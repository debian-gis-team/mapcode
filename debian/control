Source: mapcode
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Stefan Fritsch <sf@debian.org>
Section: misc
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake (>= 3.3)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/mapcode/
Vcs-Git: https://salsa.debian.org/debian-gis-team/mapcode.git
Homepage: http://www.mapcode.com/
Rules-Requires-Root: no

Package: mapcode
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Convert geo coordinates to/from mapcodes
 A mapcode represents a location. Every location on Earth can be
 represented by a mapcode. Mapcodes were designed to be short, easy to
 recognise, remember and communicate. They are precise to a few meters,
 which is good enough for every-day use. Locations in densely populated
 areas often get shorter mapcodes. See http://www.mapcode.com/
 .
 This package contains a command line utility that can convert to and
 from mapcodes.
